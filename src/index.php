<!DOCTYPE html>
<html lang="en">
<?php include("includes.php") ?>


<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 h-auto p-3">
                <div class="d-flex flex-column h-100 p-3 bg-body-secondary rounded">
                    <div class="flex-grow-1">
                        <div class="text-center pt-5 pb-3">
                            <img src="images/profile.png" class="rounded img-fluid w-75" alt="Sean O'Connor">
                        </div>
                        <div class="text-center">
                            <h2>Sean O'Connor</h2>
                            <h5>Bucknell University</h5>
                            <h5>Class of 2026</h5>
                        </div>
                    </div>
                    <div class="flex-shrink-1">
                        <hr>
                        <div class="d-flex flex-row justify-content-center">
                            <div class="px-2">
                                <a target="_blank" href="https://gitlab.com/soconnor0919" class="btn border-0 m-0 p-0">
                                    <h2>
                                        <i class="fa-brands fa-square-gitlab text-decoration-none"></i>
                                    </h2>
                                </a>
                            </div>
                            <div class="px-2">
                                <a target="_blank" href="https://github.com/soconnor0919" class="btn border-0 m-0 p-0">
                                    <h2>
                                        <i class="fa-brands fa-square-github text-decoration-none"></i>
                                    </h2>
                                </a>
                            </div>
                            <div class="px-2">
                                <a target="_blank" href="https://www.linkedin.com/in/sean-o-connor-78755a250/" class="btn border-0 m-0 p-0">
                                    <h2>
                                        <i class="fa-brands fa-linkedin text-decoration-none"></i>
                                    </h2>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 vh-100 px-0">
                <div class="d-flex flex-column h-100 p-3 ps-md-0 pt-0 pt-md-3">
                    <div class="bg-body-tertiary rounded mb-3">
                        <nav class="navbar navbar-expand-md rounded">
                            <div class="container-fluid">
                                <a class="navbar-brand d-md-none" href="#">Site Navigation</a>
                                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                        <li class="nav-item">
                                            <a class="nav-link active" aria-current="page" href="#home">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#about">About Me</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#projects">Projects</a>
                                        </li>
                                        <!-- <li class="nav-item d-md-none">
                                            <a class="nav-link" href="#">Log in</a>
                                        </li> -->
                                    </ul>
                                    <div class="d-flex d-none d-md-block">
                                        <div class="dropdown">
                                            <!-- <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                Log in
                                            </button>
                                            <ul class="dropdown-menu dropdown-menu-end p-3">
                                                <form action="backend/auth/auth.php" method="post">
                                                    <div class="form">
                                                        <input name="username" autocorrect="off" autocapitalize="none" class="form-control rounded-0 rounded-top" id="username" placeholder="Username">
                                                        <label for="floatingInput">Username</label>
                                                    </div>
                                                    <div class="form">
                                                        <input name="password" type="password" class="form-control rounded-0 rounded-bottom" id="password" placeholder="Password">
                                                        <label for="floatingPassword">Password</label>
                                                    </div>
                                                    <br>
                                                    <button class="w-100 btn btn btn-primary" type="submit">Sign in</button>
                                                </form>

                                            </ul> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <div class="bg-body-tertiary rounded flex-grow-1 p-3 overflow-scroll" data-bs-spy="scroll">
                        <h1 id="home">Hi! I'm Sean O'Connor!</h1>
                        <h3 id="about">I'm a Computer Science and Engineering student at Bucknell University.</h3>
                        <p>My interests span a variety of disciplines, including electrical/computer engineering, programming, robotics, and graphic design.<br>On this website, you can find some projects I've worked on, as well as a little more about the research I'm doing.</p>
                        <h2 id="projects">Projects</h2>
                        <div class="row">
                            <div class="col-md-4 my-3">
                                <div class="card h-100">
                                    <img src="images/nao.jpg" class="card-img-top" style="height: 180px; object-fit: cover; object-position: 50% 0;" alt="NAO Robot">
                                    <div class="card-body">
                                        <div class="d-flex flex-column h-100">
                                            <h5 class="card-title">RoboLab</h5>
                                            <p class="card-text flex-grow-1">Under the guidance of Professor Felipe Perrone, I am working on a research project exploring human-robot interaction using the Aldebaran NAO platform.</p>
                                            <a target="_blank" href="https://robolab.scholar.bucknell.edu/" class="btn btn-primary">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 my-3">
                                <div class="card h-100">
                                    <img src="images/rr.png" class="card-img-top" style="height: 180px; object-fit: cover; object-position: 50% 0;" alt="Riverhead Raceway Website">
                                    <div class="card-body">
                                        <div class="d-flex flex-column h-100">
                                            <h5 class="card-title">Riverhead Raceway</h5>
                                            <p class="card-text flex-grow-1">At home, I work for a local racetrack, handling their general IT and website needs- spanning from driver registration to backend networking.</p>
                                            <a target="_blank" href="https://riverheadraceway.com" class="btn btn-primary">Visit site</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 my-3">
                                <div class="card h-100">
                                    <img src="images/robot.png" class="card-img-top" style="height: 180px; object-fit: cover; object-position: 50% 0;" alt="Team 514 Robot">
                                    <div class="card-body">
                                        <div class="d-flex flex-column h-100">
                                            <h5 class="card-title">FIRST Robotics Competition</h5>
                                            <p class="card-text flex-grow-1">During January and Feburary, I help mentor my high school's robotics team, mainly working with control systems and programming.</p>
                                            <a target="_blank" href="https://team514.com" class="btn btn-primary">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>